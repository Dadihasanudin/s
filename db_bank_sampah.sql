-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2019 at 06:47 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bank_sampah`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_barang`
--

CREATE TABLE `tb_barang` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `foto` text NOT NULL,
  `jenis` enum('regular','eksklusif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_barang`
--

INSERT INTO `tb_barang` (`id`, `nama_barang`, `id_kategori`, `deskripsi`, `harga`, `stok`, `foto`, `jenis`) VALUES
(1, 'Seragam PDL', 2, 'Seragam luar', 50000, 15, 'pdl.jpg', 'regular'),
(2, 'Ayam Goreng Haneut', 1, 'Ayam Goreng Hangat Mantull kuk', 12001, 54, '1845312.png', 'regular');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_sampah`
--

CREATE TABLE `tb_jenis_sampah` (
  `id` int(11) NOT NULL,
  `jenis_sampah` varchar(20) NOT NULL,
  `harga` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jenis_sampah`
--

INSERT INTO `tb_jenis_sampah` (`id`, `jenis_sampah`, `harga`, `satuan`) VALUES
(1, 'botol plastik ', 5000, 'kg'),
(2, 'kardus', 1000, 'kg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id`, `nama_kategori`) VALUES
(1, 'Makanan & Minuman'),
(2, 'Pakaian');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nasabah`
--

CREATE TABLE `tb_nasabah` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `no_rekening` varchar(13) NOT NULL,
  `alamat` text NOT NULL,
  `id_sektor` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `saldo` int(11) NOT NULL,
  `point` int(11) NOT NULL,
  `status` enum('aktif','tidak_aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_nasabah`
--

INSERT INTO `tb_nasabah` (`id`, `nama_lengkap`, `no_rekening`, `alamat`, `id_sektor`, `username`, `password`, `no_telp`, `saldo`, `point`, `status`) VALUES
(1, 'Visca', '2147480000012', 'polposaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 1, 'viscanasabah', '19bdc9e76ce067fa741d0c09267ecf3d', '081111111234', 40120, 0, 'aktif'),
(2, 'eko', '606472340676', 'suk', 3, 'ekonasabah', '11ccf8f82540766c5cb01a628244246e', '081111111234', 35120, 0, 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_request`
--

CREATE TABLE `tb_request` (
  `id` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `jenis_request` enum('penukaran sampah','penukaran uang') NOT NULL,
  `status` enum('pending','diproses','menuju','selesai') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_request`
--

INSERT INTO `tb_request` (`id`, `id_nasabah`, `id_pegawai`, `jenis_request`, `status`) VALUES
(1, 1, 2, 'penukaran sampah', 'selesai'),
(2, 2, 4, 'penukaran sampah', 'selesai'),
(3, 2, 5, 'penukaran sampah', 'selesai'),
(4, 2, 5, 'penukaran sampah', 'selesai'),
(5, 1, 2, 'penukaran sampah', 'menuju');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sektor`
--

CREATE TABLE `tb_sektor` (
  `id` int(11) NOT NULL,
  `sektor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sektor`
--

INSERT INTO `tb_sektor` (`id`, `sektor`) VALUES
(1, 'babakan'),
(2, 'bumiwangi'),
(3, 'ciheulang'),
(4, 'cikoneng'),
(5, 'gunungleutik'),
(6, 'manggungharja'),
(7, 'mekar laksana'),
(8, 'mekarsari'),
(9, 'pakutandang'),
(10, 'sarimahi serangmekar'),
(11, 'sigaracipta'),
(12, 'ciparay'),
(13, 'sumbersari');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_barang`
--

CREATE TABLE `tb_transaksi_barang` (
  `id_transaksi_barang` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_biaya` int(11) NOT NULL,
  `status_pembayaran` enum('lunas','belum_lunas','','') NOT NULL,
  `status_pengantaran` enum('pending','diantar','selesai','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_barang_detail`
--

CREATE TABLE `tb_transaksi_barang_detail` (
  `id_transaksi_barang_detail` int(11) NOT NULL,
  `id_transaksi_barang` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_sampah`
--

CREATE TABLE `tb_transaksi_sampah` (
  `id_transaksi_sampah` int(11) NOT NULL,
  `id_nasabah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_sampah`
--

INSERT INTO `tb_transaksi_sampah` (`id_transaksi_sampah`, `id_nasabah`, `tanggal`, `total_harga`) VALUES
(1, 1, '2019-08-01', 25000),
(2, 2, '2019-08-02', 40000),
(3, 2, '2019-08-02', 10000),
(4, 2, '2019-08-02', 35000),
(5, 2, '2019-08-02', 35000),
(6, 1, '2019-08-02', 40000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksi_sampah_detail`
--

CREATE TABLE `tb_transaksi_sampah_detail` (
  `id_transaksi_sampah_detail` int(11) NOT NULL,
  `id_transaksi_sampah` int(11) NOT NULL,
  `id_jenis_sampah` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksi_sampah_detail`
--

INSERT INTO `tb_transaksi_sampah_detail` (`id_transaksi_sampah_detail`, `id_transaksi_sampah`, `id_jenis_sampah`, `berat`, `harga`) VALUES
(1, 1, 1, 4, 5000),
(2, 1, 2, 5, 1000),
(3, 2, 1, 8, 5000),
(4, 3, 1, 2, 5000),
(5, 5, 1, 7, 5000),
(6, 6, 1, 8, 5000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `alamat` text NOT NULL,
  `jabatan` enum('admin','petugas') NOT NULL,
  `foto` text NOT NULL,
  `status` enum('aktif','tidak_aktif') NOT NULL,
  `id_sektor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `nama_lengkap`, `username`, `password`, `no_telp`, `alamat`, `jabatan`, `foto`, `status`, `id_sektor`) VALUES
(1, 'Miqo Mahardika Aji', 'miqoadmin', 'df55868cb5f494496784a95e96351d74', '081234567899', 'Jln.Ciparay Raya Mantul', 'admin', '5530192-0023592409-tumbl.jpg', 'aktif', 10),
(2, 'Naufal Ramadhan Ku', 'naufalpetugas', '81c402ddbc018026551df68f526ea429', '081111111235', 'Jlnn.Sukarasa Leghorn58', 'petugas', '292789.jpg', 'aktif', 1),
(4, 'Budi Sudarsono', 'budipetugas', 'c3ff337566eab10b6f73717fe58e0ae2', '081234567890', 'Jln.kebayoran', 'petugas', 'jpinkman.jpg', 'aktif', 2),
(5, 'abduloh', 'abdulpetugas', 'f0dc1cd526d546ff13fbc33003b155f2', '089811111', 'polpos mania', 'petugas', 'frans1.jpg', 'aktif', 3),
(6, 'Ariana Grandee', 'arianapetugas1', 'e7b11aab31270b59a98c68a0626bb08f', '0812345', 'Amerkaa', 'petugas', 'asean.png', 'aktif', 4),
(7, 'Kevin De Bruyne', 'kevinpetugas', 'd8112d9ec097ec25e9c82bc3fd92ad0b', '081234567890', 'Jln.Ciheulang', 'petugas', 'asean1.png', 'aktif', 7);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `tb_jenis_sampah`
--
ALTER TABLE `tb_jenis_sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_request`
--
ALTER TABLE `tb_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_sektor`
--
ALTER TABLE `tb_sektor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_transaksi_barang`
--
ALTER TABLE `tb_transaksi_barang`
  ADD PRIMARY KEY (`id_transaksi_barang`);

--
-- Indexes for table `tb_transaksi_barang_detail`
--
ALTER TABLE `tb_transaksi_barang_detail`
  ADD PRIMARY KEY (`id_transaksi_barang_detail`);

--
-- Indexes for table `tb_transaksi_sampah`
--
ALTER TABLE `tb_transaksi_sampah`
  ADD PRIMARY KEY (`id_transaksi_sampah`);

--
-- Indexes for table `tb_transaksi_sampah_detail`
--
ALTER TABLE `tb_transaksi_sampah_detail`
  ADD PRIMARY KEY (`id_transaksi_sampah_detail`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_barang`
--
ALTER TABLE `tb_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_jenis_sampah`
--
ALTER TABLE `tb_jenis_sampah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_request`
--
ALTER TABLE `tb_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_sektor`
--
ALTER TABLE `tb_sektor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_transaksi_barang`
--
ALTER TABLE `tb_transaksi_barang`
  MODIFY `id_transaksi_barang` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_transaksi_barang_detail`
--
ALTER TABLE `tb_transaksi_barang_detail`
  MODIFY `id_transaksi_barang_detail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_transaksi_sampah`
--
ALTER TABLE `tb_transaksi_sampah`
  MODIFY `id_transaksi_sampah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_transaksi_sampah_detail`
--
ALTER TABLE `tb_transaksi_sampah_detail`
  MODIFY `id_transaksi_sampah_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_barang`
--
ALTER TABLE `tb_barang`
  ADD CONSTRAINT `tb_barang_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `tb_kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

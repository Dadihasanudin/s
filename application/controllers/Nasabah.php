<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nasabah extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_nasabah');
		$this->load->library('cart');
		if($this->session->userdata('status_login') != "login"){
			redirect(base_url("index.php/login/index_nasabah"));
		  }
	}

	public function getProfil()
	{
        $test = $this->session->userdata('id');
        print_r($test); die;
		$data['sektor'] = $this->model_nasabah->getSektor();

		$this->load->view('components_nasabah/header');
		$this->load->view('components_nasabah/sidebar');
		$this->load->view('nasabah/profil',$data);
		$this->load->view('components_nasabah/footer');
	}

	public function updateProfil()
	{
		$this->model_nasabah->updateProfil();
		echo"<script>alert('Profil berhasil diperbarui! Lakukan login kembali'); window.location = '../login/index_admin'</script>";
    }
    
}    
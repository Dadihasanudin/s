<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Request Panggilan</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>index.php/login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="20"><center><b>Nama Nasabah</b></center></th>
                                            <!-- <th width="20"><center><b>Alamat Nasabah</b></center></th> -->
                                            <th width="20"><center><b>Petugas</b></center></th>
                                            <th width="20"><center><b>Jenis Request</b></center></th>
                                            <th width="20"><center><b>Status</b></center></th>
                                            <th width="20"><center><b>Aksi</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($request as $request) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $request['nama_nasabah']; ?></b></center></td>
                                            <!-- <td width="20"><center><b><?= $request['alamat_nasabah']; ?></b></center></td> -->
                                            <td width="20"><center><b><?= $request['nama_petugas']; ?></b></center></td>
                                            <td width="20"><center><b><?= $request['jenis_request']; ?></b></center></td>
                                            <td width="20"><center><b><?= $request['status']; ?></b></center></td>
                                            <td width="20"><center>
                                            <a href='<?= base_url();?>index.php/admin/deleteRequest/<?= $request['id'];?>' class='btn btn-danger'><i class='fa fa-trash'></i></a>
                                            <?php if($request['status'] == 'pending'){?>
                                            <a id='btn_edit' data-id_edit='<?= $request['id']; ?>' data-id_nasabah='<?= $request['id_nasabah']; ?>' class='btn btn-info' data-toggle='modal' data-target='#updatRequest'><i class='fa fa-pencil'></i></a>
                                            <?php } ?>
                                            </center></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="updatRequest" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Update Status Request</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update" method="post" action="<?= base_url();?>index.php/admin/updateRequest">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" id="input_id">

                                    </div>
                                </div>
                            </div>  

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="input_nama_nasabah">
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="input_id_petugas">
                                    <label for="">Pilih Petugas</label>
                                    <select required class="form-control border-input"  name="id_petugas_edit" id="id_petugas_edit">
                                        
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" id="input_alamat_nasabah">
                                    
                                </div>
                            </div>
                        </div>  

                        <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Update Request
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <hr>
    </div>
</div>

<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
    'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
  ]
    } );

    $(document).on('click', '#btn_edit' ,function(){
                var id = $(this).data("id_edit");
                console.log(id)
                $.get("http://localhost/banksampah/index.php/admin/showRequest/" + id, function(data, status){
                console.log(status)
                data = JSON.parse(data);
                console.log(data)
                if (status) {
                    data.forEach(element => {
                    $('#input_id').html("")
                        $('#input_id').append("<input type='hidden' required id='id_edit' class='form-control border-input' name='id_edit' value="+element.id+">");
                    $('#input_nama_nasabah').html("")
                        $('#input_nama_nasabah').append("<label>Nama Nasabah</label><input disabled type='text' required id='nama_nasabah_edit' class='form-control border-input' name='nama_nasabah_edit' value='"+element.nama_nasabah+"'>");
                    $('#input_alamat_nasabah').html("")
                        $('#input_alamat_nasabah').append("<label for=''>Alamat Nasabah</label><textarea readonly name='alamat_nasabah_edit' id='alamat_nasabah_edit' class='form-control border-input' required cols='30' rows='10'>"+element.alamat_nasabah+"</textarea>");
                    });
                }
                else {
                    console.log('data failed')
                    }
                });

                $.ajax({
                    type: "POST",
                    data: { id_nasabah: $(this).data("id_nasabah") },
                    url: '<?php echo base_url()."index.php/admin/getPetugasSektor" ?>',
                    dataType: 'text',
                    success: function(resp) {
                    var json = JSON.parse(resp.replace(',.', ''))
                    var $el = $("#id_petugas_edit");
                    $el.empty(); // remove old options
                    $el.append($("<option></option>")
                    .attr("value", '').text('-- Pilih Petugas --'));
                    $.each(json, function(key, value) {
                        $el.append($("<option></option>")
                        .attr("value", value.id).text(value.nama_lengkap));
                    });
                    },
                    error: function (jqXHR, exception) {
                    console.log(jqXHR, exception)
                    }
                });
                
    });  

} );
</script>
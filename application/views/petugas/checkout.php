<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Checkout Transaksi Sampah</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <a href="<?= base_url();?>index.php/login/logout_admin">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content table-responsive table-full-width">
                                <table id="example" class="display nowrap" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="10"><center><b>No</b></center></th>
                                            <th width="10"><center><b>Id Cart Item</b></center></th>
                                            <!-- <th width="20"><center><b>Id Jenis Sampah</b></center></th> -->
                                            <th width="20"><center><b>Jenis Sampah</b></center></th>
                                            <th width="20"><center><b>Berat</b></center></th>
                                            <th width="20"><center><b>Satuan</b></center></th>
                                            <th width="20"><center><b>Harga</b></center></th>
                                            <th width="20"><center><b>Total</b></center></th>
                                            <th width="20"><center><b>Aksi</b></center></th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-row">
                                        <?php
                                        $no = 0;
                                        foreach ($this->cart->contents() as $items) {
                                            $no++; ?>
                                        <tr>
                                            <td width="10"><center><b><?= $no; ?></b></center></td>
                                            <td width="20"><center><b><?= $items['rowid']; ?></b></center></td>
                                            <!-- <td width="20"><center><b><?= $items['id']; ?></b></center></td> -->
                                            <td width="20"><center><b><?= $items['name']; ?></b></center></td>
                                            <td width="20"><center><b><?= $items['qty']; ?></b></center></td>
                                            <td width="20"><center><b><?= $items['satuan']; ?></b></center></td>
                                            <td width="20"><center><b><?= $items['price']; ?></b></center></td>
                                            <td width="20"><center><b><?= $items['total']; ?></b></center></td>
                                            <td width="20"><center><a href='<?=base_url();?>index.php/petugas/deleteCart/<?= $items['rowid']; ?>' class='btn btn-danger'><i class='fa fa-trash'></i></a></center></td>
                                          
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <center><h4>Sub Total</h4>
                                <h2>Rp.<?= number_format($this->cart->total(),2,',','.'); ?></h2>
                                <form action="<?= base_url();?>index.php/petugas/insertCheckout" method="post">
                                <input type="hidden" name="total" value="<?=$this->cart->total();?>"><br>
                                <input type="hidden" name="id_request" value="<?=$this->uri->segment(3);?>"><br>
                                <input type="submit" value="Checkout" class="btn btn-success btn-lg"><br></center>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          

<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {

    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
    'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
    ]
    });

    $('#id_jenis_sampah').on('change', function() {

        $.ajax({
            type: "POST",
            data: { id_jenis_sampah: $('#id_jenis_sampah').val() },
            url: '<?php echo base_url()."index.php/petugas/getHargaSampah" ?>',
            dataType: 'text',
            success: function(resp) {
            var json = JSON.parse(resp.replace(',.', ''))
            var $h = $("#harga");
            var $s = $("#satuan");
            $h.empty(); // remove old options
            $s.empty(); // remove old options
            // $h.append($("<option></option>")
            // .attr("value", '').text('-- Pilih Matpel --'));
            $.each(json, function(key, value) {
                $h.append($("<label for='' id=''>Harga</label>"))
                $h.append($("<input type='text' id='harga' name='harga' class='form-control border-input'>")
                .attr("value", value.harga));
                });
            $.each(json, function(key, value) {
                $s.append($("<label for='' id=''>Satuan</label>"))
                $s.append($("<input type='text' id='satuan' name='satuan' class='form-control border-input'>")
                .attr("value", value.satuan));
                });
            $.each(json, function(key, value) {
                $s.append($("<input type='hidden' id='jenis_sampah' name='jenis_sampah' class='form-control border-input'>")
                .attr("value", value.jenis_sampah));
                });
            },
            error: function (jqXHR, exception) {
            console.log(jqXHR, exception)
            }
        });
    });   


       

       $(function(){
        $('#berat').on('change', function(){;
            var harga = $('#harga').val();
            console.log(harga);
            var berat = $('#berat').val();
            var total = harga * berat;

            $('#total').val(total);
        });
    });

});    
</script>